import webpackConfig from './_base';

/* Prod output options */
webpackConfig.output.filename = '[name]-[hash].js';
webpackConfig.output.chunkFilename = '[name]-[chunkhash].js',
webpackConfig.output.hash = true;

export default webpackConfig;
