require('babel-register')({
    presets: ['es2015']
});

process.env.NODE_ENV = (process.env.NODE_ENV || 'development').trim();

import path from 'path';
import { argv } from 'yargs';

const config = new Map();

// Environment Globals
config.set('environment', process.env.NODE_ENV);
config.set('environmentIsProduction', (process.env.NODE_ENV === 'production'));
config.set('environment_host', (process.env.HOST || 'localhost'));
config.set('environment_port', (process.env.PORT || 3333));

config.set('environment_globals', {
    'process.env'  : {
        'NODE_ENV' : JSON.stringify(config.get('environment'))
    },
    'NODE_ENV'     : config.get('environment'),
    '__DEV__'      : config.get('environment') === 'development',
    '__PROD__'     : config.get('environment') === 'production',
    '__DEBUG__'    : config.get('environment') === 'development' && !argv.no_debug
});


// Top-level Project Paths
config.set('project_root', path.resolve(__dirname, '../'));
config.set('project_src', 'src');
config.set('project_dist', 'dist');

let directoriesToAlias = {
    actions: path.resolve(__dirname, '../src/actions'),
    reducers: path.resolve(__dirname, '../src/reducers'),
    middlewares: path.resolve(__dirname, '../src/middlewares'),
    components: path.resolve(__dirname, '../src/components'),
    containers: path.resolve(__dirname, '../src/containers'),
    routes: path.resolve(__dirname, '../src/routes'),
    styles: path.resolve(__dirname, '../src/styles')
};

config.set('resolve_aliases', directoriesToAlias);

// Vendor Dependencies
config.set('vendor_dependencies', [
    'react',
    'react-redux',
    'react-router',
    'redux',
    'lodash',
    'immutable'
]);

export default config;