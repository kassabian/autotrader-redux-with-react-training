require('babel-register')({
    presets: ['es2015']
});
import webpack from 'webpack';
import webpackConfig from './_base';

export default webpackConfig;
