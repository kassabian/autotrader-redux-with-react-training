require('babel-register')({
    presets: ['es2015']
});

import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import path from 'path';
import config from './config';
import webpackConfig from './development';

/*********************************************************************************
 * HMR Support
 *********************************************************************************/
// Add items to 'entry' array for webpack-dev-server and react hot loader
webpackConfig.entry.unshift("webpack-dev-server/client?http://0.0.0.0:8080/", "webpack/hot/only-dev-server");

// Add 'react-hot' loader to (/\.(js|jsx)$/) 'loaders' array
webpackConfig.module.loaders[0].loaders.unshift('react-hot');

console.log(webpackConfig.entry, webpackConfig.module.loaders);

// Add new webpack.HotModuleReplacementPlugin()
webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());

const compiler = webpack(webpackConfig);

const server = new WebpackDevServer(compiler, {
  contentBase: path.resolve(config.get('project_root'), config.get('project_dist')),
  hot: true,
  historyApiFallback: true
});
server.listen(8080, "0.0.0.0", function() {});
