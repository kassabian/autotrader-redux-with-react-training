require('babel-register')({
    presets: ['es2015']
});

var fs = require('fs');
var path = require('path');
var webpack = require('webpack');
import config from './config';

var CleanWebpackPlugin = require('clean-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractPlugin = require('extract-text-webpack-plugin');
var StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');


var coBrands = require('../co-brand-oems.json');
// console.log(coBrands, 999);
// var coBrandHtmlPages = [];
//
// coBrands.forEach(function(i) {
//   coBrandHtmlPages.push(
//     new HtmlWebpackPlugin({
//       // Required
//       inject: false,
//       title: i.make,
//       // template: require('html-webpack-template'),
//       template: 'node_modules/html-webpack-template/test_custom_template.ejs',
//       filename: i.make + '.html',
//       appMountId: 'app',
//       window: {
//         env: {
//           funnel: i.make
//         }
//       }
//     })
//   )
// });

// console.log(coBrandHtmlPages);




module.exports = {
    entry: [
      path.resolve(config.get('project_root'), config.get('project_src'))
    ],
    output: {
        path: path.resolve(config.get('project_root'), config.get('project_dist')),
        publicPath: "/",
        filename: 'bundle.js',
    },
    module: {
        preLoaders: [],
        loaders: [
            {
                test: /\.(js|jsx)?$/,
                exclude: /node_modules/,
                loaders: ['babel']
            },
            {
                test: /\.scss$/,
                include: config.get('resolve_aliases').styles,
                loaders: ((config.get('environmentIsProduction')) ? ExtractPlugin.extract('style', 'css!sass') : ['style', 'css', 'sass'])
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'url?limit=6144',
                    'file?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },

    plugins: [
        new CleanWebpackPlugin(['dist'], {
          root: config.get('project_root'),
          verbose: true,
          dry: false
        }),
        new HtmlWebpackPlugin({
          // Required
          inject: false,
          template: require('html-webpack-template'),
          //template: 'node_modules/html-webpack-template/index.ejs',
          filename: 'index.html',
          appMountId: 'app',
          window: {
            env: {
              funnel: 'kbb'
            }
          }
        }),
        new HtmlWebpackPlugin({
          // Required
          inject: false,
          title: 'ATC APP TITLE',
          // template: require('html-webpack-template'),
          template: 'node_modules/html-webpack-template/test_custom_template.ejs',
          filename: 'atc.html',
          appMountId: 'app',
          window: {
            env: {
              funnel: 'atc'
            }
          }
        }),
        // ...coBrandHtmlPages
        new StaticSiteGeneratorPlugin('')
    ]
};
