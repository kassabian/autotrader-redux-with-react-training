require('babel-register');
import {argv} from 'yargs';

let buildType;

switch (argv.buildType) {
    case 'dev':
        buildType = 'development';
        break;
    default:
        buildType = 'production';
        break;
}
module.exports = require('./build/' + buildType);
