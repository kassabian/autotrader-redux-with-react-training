import React from 'react';
import ReactDOM from 'react-dom';

export default class App extends React.Component {
  render() {
    return (
      <h1>Hey, {this.props.funnel}!</h1>
    );
  }
}
