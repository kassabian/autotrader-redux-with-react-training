import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';

if(window.env) {
  console.log(window.env.funnel);
}

class Root extends React.Component {
  render() {
    return (
      <App funnel={window.env.funnel} />
    );
  }
}

ReactDOM.render(<Root />, document.getElementById('app'));
