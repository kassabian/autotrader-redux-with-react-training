import React from 'react';
import ReactDOM from 'react-dom';
import App from '../app';

class Root extends React.Component {
  render() {
    return (
      <App funnel="atc" />
    );
  }
}

ReactDOM.render(<Root />, document.getElementById('app'));
